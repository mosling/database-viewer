#ifndef QEPROGRESSBAR_H
#define QEPROGRESSBAR_H

#include <QProgressBar>
#include "ui_QeProgressBar.h"
#include "CtProgressInterface.h"

class QeProgressBar : public QProgressBar, public Ui_QeProgressBarClass, public CtProgressInterface
{
    Q_OBJECT

public:
    QeProgressBar(QWidget *parent = 0);
    ~QeProgressBar();
	 virtual void setProgress(int value);
	 virtual void setMaxProgress(int value);

public slots:
	void centerParent();

private:
	QWidget *parent;
};

#endif // QEPROGRESSBAR_H
