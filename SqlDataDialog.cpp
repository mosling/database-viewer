#include "SqlDataDialog.h"
#include <QPushButton>

SqlDataDialog::SqlDataDialog(QWidget *parent) : QDialog(parent),
	sqlDataMapper(NULL)
{

}

SqlDataDialog::~SqlDataDialog()
{

}

void SqlDataDialog::setDataMapper(SqlDataMapper *aMapper)
{
	sqlDataMapper = aMapper;
	connectControl();
}

void SqlDataDialog::connectControl()
{
	QList<QPushButton*> bl;
	bl = this->findChildren<QPushButton*>("btnNext");
	if (bl.size() == 1)
	{
		QObject::connect(bl[0], SIGNAL(clicked())
			, sqlDataMapper, SLOT(next()));
	}
	bl.clear();
	bl = this->findChildren<QPushButton*>("btnPrev");
	if (bl.size() == 1)
	{
		QObject::connect(bl[0], SIGNAL(clicked())
			, sqlDataMapper, SLOT(previous()));
	}
		bl.clear();
	bl = this->findChildren<QPushButton*>("btnFirst");
	if (bl.size() == 1)
	{
		QObject::connect(bl[0], SIGNAL(clicked())
			, sqlDataMapper, SLOT(first()));
	}
		bl.clear();
	bl = this->findChildren<QPushButton*>("btnLast");
	if (bl.size() == 1)
	{
		QObject::connect(bl[0], SIGNAL(clicked())
			, sqlDataMapper, SLOT(last()));
	}

}
