#-------------------------------------------------
#
# Project created by QtCreator 2013-02-28T08:28:20
#
#-------------------------------------------------

QT       += core gui sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = qtdb
TEMPLATE = app

include (../../moonwave.pri)

SOURCES += main.cpp \
    SqlDataMapperRelation.cpp \
    SqlDataMapperField.cpp \
    SqlDataMapper.cpp \
    SqlDataDialog.cpp \
    qtdb.cpp \
    QLocalizationDialog.cpp \
    QeProgressBar.cpp \
    MySqlTableModel.cpp \
    ChipDialog.cpp \
    PropertyDialog.cpp

HEADERS += \
    SqlDataMapperRelation.h \
    SqlDataMapperField.h \
    SqlDataMapper.h \
    SqlDataDialog.h \
    qtdb.h \
    QLocalizationDialog.h \
    QeProgressBar.h \
    MySqlTableModel.h \
    CtProgressInterface.h \
    ChipDialog.h \
    PropertyDialog.h

FORMS += \
    qtdb.ui \
    QLocalizationDialog.ui \
    QeProgressBar.ui \
    QDialogFrame.ui \
    ChipDialog.ui \
    PropertyDialog.ui
