#include "SqlDataMapperRelation.h"
#include <QtSql/QSqlRecord>

SqlDataMapperRelation::SqlDataMapperRelation(QComboBox *parent)
	: QObject(parent), cbField(parent), useMapping(false)
{

}

SqlDataMapperRelation::~SqlDataMapperRelation()
{
	mapIndex.clear();
}

void SqlDataMapperRelation::showIndex(QVariant aKey)
{
	int cbIdx;

	if (useMapping)
	{
		int idx = aKey.toInt();
		cbIdx = mapIndex[idx];
	}
	else
	{
		QString str = aKey.toString();
		cbIdx = cbField->findText(str);
	}

	cbField->setCurrentIndex(cbIdx);
}

/*!
Diese Funktions führt die SQL-Anweisung aus und
setzt die Werte in der zugeordneten Kombobox.
*/
void SqlDataMapperRelation::setRelation(QString aQuery)
{
	QSqlQuery query;
	int cbIdx = 1;

	cbField->addItem(""); // spezial item used as NULL-value
	query.exec(aQuery);
	int numCols = -1;
	if (query.isActive())
	{
		QSqlRecord rec = query.record();
		numCols = rec.count();
		if (numCols > 1)
		{
			// Variante 1: PK, Anzeigewerte
			// TODO: prüfen on col(0) auch eine Zahl ist
			while (query.next())
			{
				mapIndex[query.value(0).toInt()] = cbIdx;
				QString strTmp = query.value(1).toString();
				int j = 2;
				while ( j < numCols) strTmp += query.value(j++).toString();
				cbField->addItem(strTmp);
				cbIdx++;
			}
			useMapping = true;
		}
		else
		{
			// Variante 2: nur PK vorhanden, Auswahlliste
			while (query.next())
			{
				cbField->addItem(query.value(0).toString());
				cbIdx++;
			}
		}
	}
}
