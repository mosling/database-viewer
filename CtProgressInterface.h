#ifndef _CTPROGRESSINTERFACE_
#define _CTPROGRESSINTERFACE_

class CtProgressInterface
{
public:
    virtual void setProgress(int value)=0;
    virtual void setMaxProgress(int value)=0;
};

#endif
