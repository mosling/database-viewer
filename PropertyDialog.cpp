//! MOONWAVE SYSTEM GMBH
//! copyright 2013

#include "PropertyDialog.h"
#include "ui_PropertyDialog.h"

PropertyDialog::PropertyDialog(QWidget *parent) :
	SqlDataDialog(parent),
	ui(new Ui::PropertyDialog)
{
	ui->setupUi(this);
	ui->frameMaster->setWhatsThis("property");
	ui->frameMaster->setStatusTip("master");

	ui->lineEditId->setWhatsThis("id");
	ui->lineEditId->setStatusTip("PK");

	ui->lineEditName->setWhatsThis("name");

	ui->comboBoxNumType->setWhatsThis("fk_num_type");
	ui->comboBoxNumType->setStatusTip("FK;num_type;id;name");

	ui->comboBoxClass->setWhatsThis("fk_class");
	ui->comboBoxClass->setStatusTip("FK;class;id;name");

	ui->comboBoxEnum->setWhatsThis("fk_enum");
	ui->comboBoxEnum->setStatusTip("FK;enum;id;name");
}

PropertyDialog::~PropertyDialog()
{
	delete ui;
}
