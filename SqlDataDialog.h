#ifndef SQLDATADIALOG_H
#define SQLDATADIALOG_H

#include <QDialog>
#include "SqlDataMapper.h"

class SqlDataDialog : public QDialog
{
	Q_OBJECT

public:
	SqlDataDialog(QWidget *parent = 0);
	~SqlDataDialog();

	void setDataMapper(SqlDataMapper *aMapper);

protected:
	SqlDataMapper *sqlDataMapper;

	void connectControl();
	
};

#endif // SQLDATADIALOG_H
