#ifndef QTDB_H
#define QTDB_H

#include <QMainWindow>
#include <QGridLayout>
#include <QtSql>
#include <QList>
#include <QSortFilterProxyModel>
#include <QStringListModel>
#include <QStringList>

// forward declaration
namespace Ui {
class qtdb;
}

class qtdb : public QMainWindow
{
	Q_OBJECT

public:
    qtdb(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~qtdb();

protected:
	void showError(QString err, bool withClear=false);
	bool checkOpen();
	void createInsertFormular();
	bool needQuotation(QVariant::Type t);
	bool useTransaction();
	void clearWidgetList();
	void setTransaction(QString aState);

private:
	Ui::qtdb *ui;
	QSqlDatabase db;
	QSqlRelationalTableModel *sqlModel;
	QString tableName;
	QSortFilterProxyModel *sqlProxy;
	QList<QWidget*> columnWidgetList;
	QSqlRecord tableRecord;
	QGridLayout *columnGridLayout;
	QStringListModel *tableListModel;
	QStringList lookupList;

private slots:
	void on_btnBegin_clicked();
	void on_btnCommit_clicked();
	void on_btnRollback_clicked();
	void on_btnLocalization_clicked();
	void on_pbChipDialog_clicked();
	void on_btnInfo_clicked();
	void on_cbConnected_stateChanged(int);
	void on_btnFilter_clicked();
	void on_btnInsert_clicked();
	void on_btnPaths_clicked();
	void on_btnShow_clicked();
	void on_btnClose_clicked();
	void on_btnConnect_clicked();
	void on_btnAdd_clicked();
};

#endif // QTDB_H
