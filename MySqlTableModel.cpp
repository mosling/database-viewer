#include "MySqlTableModel.h"
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlIndex>


MySqlTableModel::MySqlTableModel(QObject *parent)
	: QSqlRelationalTableModel(parent)
{

}

MySqlTableModel::~MySqlTableModel()
{

}

bool MySqlTableModel::select()
{
	return QSqlTableModel::select();
}

//bool MySqlTableModel::select(QString query)
//{
//	QSqlTableModelPrivate *d = (QSqlTableModelPrivate *)(d_ptr);
//	if (query.isEmpty())
//		return false;

//	revertAll();
//	QSqlQuery qu(query, d->db);
//	setQuery(qu);

//	if (!qu.isActive()) {
//		// something went wrong - revert to non-select state
//		setTable(d->tableName);
//		return false;
//	}
//	return true;
//}

