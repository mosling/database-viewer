#include "QLocalizationDialog.h"
#include <QtSql/QSqlError>
#include <QtSql/QSqlDatabase>

QLocalizationDialog::QLocalizationDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

QLocalizationDialog::~QLocalizationDialog()
{

}

/*!
Mit dieser Methode wird die aktuelle Id auf den übergebenen
Wert gesetzt.
*/
void QLocalizationDialog::setLocalizationId(int vId)
{
	QString sql = QString("select count(*) from vt_localization where localization_id = %1").arg(vId);
	QSqlQuery query;

	query.exec(sql);
	query.next();
	int numRows = query.value(0).toInt();
	currId = vId;

	if (numRows > 0)
	{
                sql = QString("select fk_language, message from vt_localization where localization_id = %1").arg(vId);
		query.exec(sql);

		while (query.next())
		{
			if (query.value(0).toString() == "DE")
			{
				ui.te_deutsch->setText(query.value(1).toString());
			}
			else if (query.value(0).toString() == "EN")
			{
				ui.te_english->setText(query.value(1).toString());
			}
		}
	}
}

void QLocalizationDialog::on_cbEN_stateChanged(int)
{
	
}


void QLocalizationDialog::on_btnShow_clicked()
{
	setLocalizationId(ui.spLocId->value());
}

void QLocalizationDialog::on_btnUpdate_clicked()
{
	QSqlQuery query;
	QString sql;
	QString de = ui.te_deutsch->toPlainText();
	QString en = ui.te_english->toPlainText();
	bool ok;
	QSqlDatabase db = QSqlDatabase::database();

        sql = QString("update vt_localization set message='%1' where localization_id=%2 and fk_language='DE'").arg(de).arg(currId);
	ok = query.exec(sql);

	if (!ok)
	{
		QSqlError sqlerr = QSqlDatabase::database().lastError();
		QString err = sqlerr.text();
	}

        sql = QString("update vt_localization set message='%1' where localization_id=%2 and fk_language='EN'").arg(en).arg(currId);
	ok = query.exec(sql);
	if (!ok)
	{
		QSqlError sqlerr = QSqlDatabase::database().lastError();
		QString err = sqlerr.text();
	}

}

void QLocalizationDialog::on_btnNew_clicked()
{
	QSqlQuery query;
	QSqlDatabase db = QSqlDatabase::database();

	query.exec("select max(localization_id)+1 from vt_localization");
	query.next();
	int newLocId = query.value(0).toInt();
	ui.spLocId->setValue(newLocId);

	query.exec(QString("insert into vt_localization (fk_language, localization_id) "
		" values ('DE', %1)").arg(newLocId));
	db.commit();

	query.exec(QString("insert into vt_localization (fk_language, localization_id) "
		" values ('EN', %1)").arg(newLocId));
	db.commit();
}
