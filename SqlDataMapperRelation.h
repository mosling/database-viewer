#ifndef SQLDATAMAPPERRELATION_H
#define SQLDATAMAPPERRELATION_H

#include <QObject>
#include <QComboBox>
#include <QtSql/QSqlQuery>

/*!
Diese Klasse verwaltet die Zuordnung einer Lookup-Tabelle
zu einer KomboBox.
*/
class SqlDataMapperRelation : public QObject
{
	Q_OBJECT

public:
	SqlDataMapperRelation(QComboBox *parent);
	~SqlDataMapperRelation();

	void setRelation(QString aQuery);
	void showIndex(QVariant aKey);

private:
	QMap<int, int> mapIndex; //!< Abbildung des PK-Tabelle zum CB-Index
	QComboBox *cbField;
	bool useMapping;
	
};

#endif // SQLDATAMAPPERRELATION_H
