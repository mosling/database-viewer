#ifndef SQLDATAMAPPERFIELD_H
#define SQLDATAMAPPERFIELD_H

#include <QObject>
#include "SqlDataMapperRelation.h"

class SqlDataMapperField : public QObject
{
	Q_OBJECT

public:
	SqlDataMapperField(QObject *parent);
	~SqlDataMapperField();

	enum etFieldType {NoType, LineEdit, ComboBox};
	
	etFieldType fieldType;
	QWidget *fieldWidget;
	int fieldIndex;
	SqlDataMapperRelation *fieldRelation;


private:
	
};

#endif // SQLDATAMAPPERFIELD_H
