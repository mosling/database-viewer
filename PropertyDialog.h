//! MOONWAVE SYSTEM GMBH
//! copyright 2013

#pragma once

#include "SqlDataDialog.h"
#include <QWidget>

namespace Ui {
class PropertyDialog;
}

class PropertyDialog : public SqlDataDialog
{
	Q_OBJECT

public:
	explicit PropertyDialog(QWidget *parent = 0);
	~PropertyDialog();

private:
	Ui::PropertyDialog *ui;
};

