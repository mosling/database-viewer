#ifndef MYSQLTABLEMODEL_H
#define MYSQLTABLEMODEL_H

#include <QtSql/QSqlRelationalTableModel>

class MySqlTableModel : public QSqlRelationalTableModel
{
	Q_OBJECT

public:
	MySqlTableModel(QObject *parent = NULL);
	~MySqlTableModel();
	bool select();
        bool select(QString query);

private:
	
};

#endif // MYSQLTABLEMODEL_H
