#include "QeProgressBar.h"

QeProgressBar::QeProgressBar(QWidget *parent)
    : QProgressBar(parent)
{
    setupUi(this);
	 this->parent=parent;
	 setMinimum(0);
	 if (parent)
	 {
		 centerParent();
	 }
}

QeProgressBar::~QeProgressBar()
{
}

void QeProgressBar::centerParent()
{
	if (parent!=NULL)
	{
		int xmid=parent->width()/2;
		int ymid=parent->height()/2;
		int w=400;
		int h=25;
		if (w>parent->width())
		{
			w = parent->width();
		}
		this->setGeometry(xmid-w/2,ymid-h/2,w,h);
	}
}

void QeProgressBar::setProgress(int value)
{
	setValue(value);
}

void QeProgressBar::setMaxProgress(int value)
{
	setMaximum(value);
}
