#include "ChipDialog.h"
#include "ui_ChipDialog.h"

ChipDialog::ChipDialog(QWidget *parent)
	: SqlDataDialog(parent),
	  ui(new Ui::ChipDialog)
{
	ui->setupUi(this);
}

ChipDialog::~ChipDialog()
{
	delete ui; ui = NULL;
}
