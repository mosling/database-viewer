﻿#include <QtSql/QSqlQuery>
#include <QHeaderView>
#include <windows.h>
#include <QMessageBox>
#include "qtdb.h"
#include "ui_qtdb.h"
#include "QeProgressBar.h"

#include "SqlDataMapper.h"
#include "PropertyDialog.h"
#include "ChipDialog.h"
#include "QLocalizationDialog.h"

qtdb::qtdb(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags),
	  ui(new Ui::qtdb),
	  sqlModel(NULL),
	  tableName(""),
	  sqlProxy(NULL),
	  columnGridLayout(NULL),
	  tableListModel(NULL)
{
	ui->setupUi(this);

	// model for the dynamic table list
	tableListModel = new QStringListModel(this);

	//	// the proxy for the table model
	//	sqlProxy = new QSortFilterProxyModel();
	//	ui->tvResult->setModel(sqlProxy);
	//	connect(ui->tvResult->horizontalHeader(),SIGNAL(sectionClicked(int)),
	//			ui->tvResult,SLOT(sortByColumn(int)));
	//	//ui->tvResult->verticalHeader()->hide();
	//	ui->tvResult->horizontalHeader()->setSortIndicatorShown(true);
	//	ui->tvResult->horizontalHeader()->setStretchLastSection(true);

	// Combobox füllen
	ui->cbDbType->addItem("QODBC");
	ui->cbDbType->addItem("QPSQL");
	ui->cbDbType->addItem("QMYSQL");
	ui->cbDbType->addItem("QSQLITE");

	QSettings rc("Torqeedo","qtdb");

	this->restoreState(rc.value("properties").toByteArray());
	// reset size and position of the main window
	int x=rc.value("window_xpos","-1").toInt();
	int y=rc.value("window_ypos","-1").toInt();
	int w=rc.value("window_width","-1").toInt();
	int h=rc.value("window_height","-1").toInt();
	if (x!=-1 && y!=-1 && w!=-1 && h!=-1)
	{
		this->frameGeometry().setRect(x,y,w,h);
	}
	// restore the saved test field values
	ui->cbDbType->setCurrentText(rc.value("db.type", "QPSQL").toString());
	ui->leDbHost->setText(rc.value("db.host","localhost").toString());
	ui->leDbName->setText(rc.value("db.name","").toString());
	ui->leDbPort->setText(rc.value("db.port","5432").toString());
	ui->leDbPassword->setText(rc.value("db.password","postgres").toString());
	ui->leDbUser->setText(rc.value("db.user","postgres").toString());

	// prepare the layout for the dynamin form
	columnGridLayout = new QGridLayout(ui->frameColumns);
}

qtdb::~qtdb()
{
	QSettings rc("Torqeedo","qtdb");

	rc.setValue("db.type", ui->cbDbType->currentText());
	rc.setValue("db.name", ui->leDbName->text());
	rc.setValue("db.host", ui->leDbHost->text());
	rc.setValue("db.port", ui->leDbPort->text());
	rc.setValue("db.password", ui->leDbPassword->text());
	rc.setValue("db.user", ui->leDbUser->text());

	rc.setValue("properties", QVariant(this->saveState()));
	rc.setValue("window_xpos",QVariant(this->x()));
	rc.setValue("window_ypos",QVariant(this->y()));
	rc.setValue("window_width",QVariant(this->frameGeometry().width()));
	rc.setValue("window_height",QVariant(this->frameGeometry().height()));

	on_btnClose_clicked();
	delete sqlModel;
	delete sqlProxy;
	clearWidgetList();
	delete tableListModel;
	delete ui;
}

void qtdb::clearWidgetList()
{
	if (columnWidgetList.count() > 0)
	{
		qDeleteAll(columnWidgetList);
		columnWidgetList.clear();
	}
}

bool qtdb::checkOpen()
{
	bool success = true;
	if (!db.isOpen())
	{
		QMessageBox::warning(this,"DB Error","database not open",QMessageBox::Ok);
		success = false;
	}
	return success;
}

void qtdb::showError(QString err, bool withClear)
{
	if (withClear) ui->teInfo->clear();
	ui->teInfo->append("ERROR "+err);
	if (db.lastError().isValid())
	{
		ui->teInfo->append(db.lastError().text());
	}
	ui->teInfo->append("------------------");
}

/*!
Anzeige der Tabelle in der QTableView als QSqlTableModel.
*/
void qtdb::on_btnShow_clicked()
{
	if (checkOpen())
	{
		on_btnInfo_clicked();
		sqlModel->setTable(tableName);

		if (lookupList.size() > 0)
		{
			QSqlRecord tabrec = db.record(tableName);
			foreach (QString str, lookupList)
			{
				QStringList l = str.split(",", QString::SkipEmptyParts);
				if (l.size() == 4)
				{
					sqlModel->setRelation(tabrec.indexOf(l.at(0)),
										  QSqlRelation(l.at(1), l.at(2), l.at(3)));
				}
			}
		}

		sqlModel->select();
	}
}

void qtdb::on_btnConnect_clicked()
{
	db = QSqlDatabase::addDatabase(ui->cbDbType->currentText());
	db.setDatabaseName(ui->leDbName->text());
	db.setHostName(ui->leDbHost->text());
	db.setPort(ui->leDbPort->text().toInt());
	db.setUserName(ui->leDbUser->text());
	db.setPassword(ui->leDbPassword->text());

	bool ok = db.open();
	if (ok == true)
	{
		bool b;

		if (db.driver()->hasFeature(QSqlDriver::Transactions))
		{
			ui->cbTransaction->setEnabled(true);
		}
		QString features[] = {"Transactions","QuerySize","BLOB","Unicode",
							  "PreparedQueries","NamedPlaceHolders","PositionalPlaceHolders",
							  "LastInsertId", "BatchOperations", "SimpleLocking",
							  "LowPrecisionNumbers", "EventNotification", "FinishQuery",
							  "MultipleResultSets", ""};
		ui->cbConnected->setCheckState(Qt::Checked);
		QString s;

		ui->teInfo->clear();
		for (int i=0; !features[i].isEmpty(); ++i)
		{
			s = features[i]+": ";
			b = db.driver()->hasFeature((QSqlDriver::DriverFeature)i);
			s+=b?"yes":"no";
			ui->teInfo->append(s);
		}

		QStringList tableList = db.tables();
		ui->teInfo->append("Tablelist: ");
		foreach (QString tn, tableList)
		{
			ui->teInfo->append(tn);
		}
		tableListModel->setStringList(tableList);
		ui->cbTable->setModel(tableListModel);

		// create the model and bind it to the table view, setting left join
		// mode, because we will not loose data by NULL values in foreign key
		// fields.
		sqlModel = new QSqlRelationalTableModel(this);
		sqlModel->setJoinMode(QSqlRelationalTableModel::LeftJoin);

		ui->tvResult->setModel(sqlModel);
		//sqlProxy->setSourceModel(sqlModel);
		ui->tvResult->setItemDelegate(new QSqlRelationalDelegate(ui->tvResult));
	}
	else
	{
		showError("connect",true);
	}
}

void qtdb::on_btnClose_clicked()
{
	if (db.isOpen())
	{
		db.close();
	}
	ui->cbConnected->setCheckState(Qt::Unchecked);
	ui->cbTransaction->setCheckState(Qt::Unchecked);
	ui->cbTransaction->setEnabled(false);
	clearWidgetList();
}

bool qtdb::useTransaction()
{
	return ui->cbTransaction->isChecked();

}

void qtdb::on_btnPaths_clicked()
{
	QStringList l = QApplication::libraryPaths();
	ui->teInfo->append("library paths: ");
	foreach (QString s, l) ui->teInfo->append(s);
}

void qtdb::setTransaction(QString aState)
{
	bool b = true;

	if (checkOpen())
	{
		if (aState == "begin")
		{
			b = db.transaction();
			if (b)
			{
				ui->lblTransactionStatus->setText("Transaction started");
			}
		}
		else if (aState == "commit")
		{
			b = db.commit();
			if (b)
			{
				ui->lblTransactionStatus->setText("Transaction commited");
			}

		}
		else if (aState == "rollback")
		{
			b = db.rollback();
			if (b)
			{
				ui->lblTransactionStatus->setText("Transaction rollback");
			}
		}
		if (!b) showError(aState);
	}
}

void qtdb::on_btnBegin_clicked()
{
	setTransaction("begin");
}

void qtdb::on_btnCommit_clicked()
{
	setTransaction("commit");
}

void qtdb::on_btnRollback_clicked()
{
	setTransaction("rollback");
}

/*!
Falls es sich um einen Zeichenkettentyp handelt liefert die Methode
true.
*/
bool qtdb::needQuotation(QVariant::Type t)
{
	bool res = false;
	if (t == QVariant::String || t == QVariant::Date || t == QVariant::Time
			|| t == QVariant::DateTime)
	{
		res = true;
	}
	return res;
}

/*!
Anhand der Struktur in tableRecord und der vorhandenen Widgetliste erzeugen
wir ein insert-Statement.
*/
void qtdb::on_btnInsert_clicked()
{
	if (checkOpen() && !tableRecord.isEmpty())
	{
		if (columnWidgetList.count() == tableRecord.count()*2)
		{
			QString sql = "Insert into "+tableName+" (";
			QString val = " values (";
			QString v;
			for (int c=0; c<tableRecord.count(); ++c)
			{
				v = "";
				QSqlField f = tableRecord.field(c);
				switch (f.type())
				{
				case QVariant::Bool:
				{
					QCheckBox *cb = (QCheckBox*)columnWidgetList.at(c*2+1);
					v = (cb->isChecked()?"'1'":"'0'");
				}
					break;
				case QVariant::Int:
				{
					bool bOk;
					int vNum;
					QString vStr = ((QLineEdit*)columnWidgetList.at(c*2+1))->text();
					vStr.remove('_');
					if (vStr.contains("x"))
					{
						vNum = vStr.toInt(&bOk, 16);
					}
					else if (vStr.startsWith("0"))
					{
						vNum = vStr.toInt(&bOk, 8);
					}
					else
					{
						vNum = vStr.toInt(&bOk);
					}
					if (bOk)
					{
						v = QString("%1").arg(vNum);
					}
					else
					{
						// error
						v = "";
					}
				}
					break;
				default:
				{
					QLineEdit *le = (QLineEdit*)columnWidgetList.at(c*2+1);
					if (le->text().length() > 0)
					{
						if (needQuotation(f.type()))
						{
							v = "'"+le->text()+"'";
						}
						else
						{
							v = le->text();
						}
					}
				}
				} // switch

				if (v.length() > 0)
				{
					sql += f.name() + ",";
					val += v + ",";
				}
			}

			sql.remove(sql.length()-1,1);
			val.remove(val.length()-1,1);
			sql += ") "+val+")";

			ui->teInfo->append(sql);
			QSqlQuery q;
			if (useTransaction())
			{
				if (!db.transaction()) showError("start transaction");
				if (!q.exec(sql)) showError("insert");
				if (!db.commit()) showError("commit");
			}
			else
			{
				if (!q.exec(sql)) showError("insert");
			}
		}
	}
}

/*!
Bei einem Filter benötigen wir nur eine Whereklausel. Diese 
wird durch ein Query by example erstellt.
*/
void qtdb::on_btnFilter_clicked()
{
	if (checkOpen() && !tableRecord.isEmpty())
	{
		if (columnWidgetList.count() == tableRecord.count()*2)
		{
			QString sql,v;
			for (int c=0; c<tableRecord.count(); ++c)
			{
				v = "";
				QSqlField f = tableRecord.field(c);
				switch (f.type())
				{
				case QVariant::Bool:
				{
					QCheckBox *cb = (QCheckBox*)columnWidgetList.at(c*2+1);
					if (cb->checkState() != Qt::PartiallyChecked)
					{
						v = (cb->isChecked()?"'1'":"'0'");
					}
				}
					break;
				default:
				{
					QLineEdit *le = (QLineEdit*)columnWidgetList.at(c*2+1);
					if (le->text().length() > 0)
					{
						if (needQuotation(f.type()))
						{
							v = "'"+le->text()+"'";
						}
						else
						{
							v = le->text();
						}
					}
				}
				} // switch

				if (v.length() > 0)
				{
					QString op = "=";
					if (v.contains("%")) op = " like ";
					if (sql.length() > 0) sql += " and ";
					sql += f.name() + op + v;
				}
			}

			ui->teInfo->append(sql);
			sqlModel->setFilter(sql);
			sqlModel->select();
		}
	}
}

/*!
Diese Methode ist notwendig, da es keine andere Möglichkeit gab die Checkbox
vor dem Nutzer zu schützen und trotzdem vom Programm zu verwenden.
*/
void qtdb::on_cbConnected_stateChanged(int)
{
	ui->cbConnected->setCheckState(db.isOpen()?Qt::Checked:Qt::Unchecked);
}

//! Show some information about the selected table.
void qtdb::on_btnInfo_clicked()
{
	if (checkOpen())
	{
		tableName = ui->cbTable->currentText();
		lookupList.clear();
		ui->teInfo->append("info about "+tableName+" :");
		tableRecord = db.record(tableName);
		ui->teInfo->append(QString(" %1 columns").arg(tableRecord.count()));
		if (!tableRecord.isEmpty())
		{
			for (int c=0; c<tableRecord.count(); ++c)
			{
				QSqlField f = tableRecord.field(c);
				ui->teInfo->append(QString(" %1: %2 %3 %4,%5 %6 %7 %8 %9")
								  .arg(c)
								  .arg(f.name())
								  .arg(QVariant::typeToName(f.type()))
								  .arg(f.length())
								  .arg(f.precision())
								  .arg(f.isNull()?"NULL":"")
								  .arg(f.defaultValue().toString())
								  .arg(f.isAutoValue()?"serial":"")
								  .arg(f.requiredStatus()==QSqlField::Required?"required":(f.requiredStatus()==QSqlField::Optional?"optional":""))
								  );
			}
			QSqlIndex index = db.primaryIndex(tableName);
			if (!index.isEmpty())
			{
				ui->teInfo->append(QString(" primary key %1").arg(index.name()));
				for (int c=0; c<index.count(); ++c)
				{
					QSqlField f = index.field(c);
					ui->teInfo->append(QString(" %1: %2")
									  .arg(c)
									  .arg(f.name())
									  );
				}
			}
			else
			{
				ui->teInfo->append("No primary key information available");
			}

			// look for an existing lookup table
			QSqlRecord lookupRecord = db.record("lookup");
			if (!lookupRecord.isEmpty())
			{
				QString lookupQueryStr("select fk_table, fk_column, pk_table, "
									  "pk_column, pk_display_column "
									  "from lookup "
									  "where fk_table='" + tableName + "' "
									  "order by fk_table, fk_column");

				bool lookupAvailable = false;

				QSqlQuery lookupQuery(lookupQueryStr);
				while (lookupQuery.next())
				{
						QString lstr = QString("%1,%2,%3,%4")
									   .arg(lookupQuery.value(1).toString())
									   .arg(lookupQuery.value(2).toString())
									   .arg(lookupQuery.value(3).toString())
									   .arg(lookupQuery.value(4).toString());

						lookupList.append(lstr);
						ui->teInfo->append(lstr);

						lookupAvailable = true;
				}
				if (!lookupAvailable)
				{
					ui->teInfo->append("No lookup informations available");
				}
			}
			else
			{
				ui->teInfo->append("no lookup table available to detemine lookup values");
			}
		}
		createInsertFormular();
	}
}

/*!
Aus einem vorhandenen QSqlRecord wird ein Eingabeformular erstellt.
*/
void qtdb::createInsertFormular()
{
	if (checkOpen() && !tableRecord.isEmpty() )
	{
		clearWidgetList();

		// Jetzt die Eingabefelder erzeugen
		for (int c=0; c<tableRecord.count(); ++c)
		{
			QSqlField f = tableRecord.field(c);
			QLabel *label = new QLabel(ui->frameColumns);
			label->setText(QApplication::translate(tableName.toLatin1(),
												   f.name().toLatin1()));
			columnGridLayout->addWidget(label, c, 0);
			columnWidgetList.append(label);
			switch (f.type())
			{
			case QVariant::Bool:
				// create an CheckBox
			{
				QCheckBox *cb = new QCheckBox(ui->frameColumns);
				cb->setTristate();
				columnGridLayout->addWidget(cb, c, 1);
				columnWidgetList.append(cb);
				break;
			}
			default:
				// create an line edit field
			{
				QLineEdit *le = new QLineEdit(ui->frameColumns);
				columnGridLayout->addWidget(le, c, 1);
				columnWidgetList.append(le);
				break;
			}
			}
		}
		ui->frameColumns->repaint();
	}
}


void qtdb::on_pbChipDialog_clicked()
{
	PropertyDialog *cd = 	new PropertyDialog(this);
	// ChipDialog *cd = 	new ChipDialog(this);
	SqlDataMapper *sqldm = new SqlDataMapper(this);

	cd->show();

	sqldm->setDialog(cd);
	if (!sqldm->initDialog())
	{
		showError("init chip dialog");
	}
	else
	{
		cd->setDataMapper(sqldm);
		cd->exec();
	}

	delete sqldm;
	delete cd;
}

void qtdb::on_btnLocalization_clicked()
{
	QLocalizationDialog diaLoc(this);
	
	diaLoc.show();
	diaLoc.exec();

}

void qtdb::on_btnAdd_clicked()
{
	if (NULL != sqlModel)
	{
		sqlModel->insertRow(sqlModel->rowCount());
	}
}
