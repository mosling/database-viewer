#include "qtdb.h"

#include <QApplication>
#include <QStyleFactory>

#include <iostream>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));

	// start application
	// set the style QStyleFactory::keys()
	QStringList sl = QStyleFactory::keys();
	// typically one of "windows", "motif", "cde",
	// "plastique", "windowsxp", or "cleanlooks".
	QApplication::setStyle("plastique");
	qtdb w;

	w.show();
	return a.exec();
}
