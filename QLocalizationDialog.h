#ifndef QLOCALIZATIONDIALOG_H
#define QLOCALIZATIONDIALOG_H

#include <QDialog>
#include <QtSql/QSqlQuery>
#include "ui_QLocalizationDialog.h"

class QLocalizationDialog : public QDialog
{
	Q_OBJECT

public:
	QLocalizationDialog(QWidget *parent = 0);
	~QLocalizationDialog();

	void setLocalizationId(int vId);

private:
	Ui::QLocalizationDialogClass ui;
	unsigned int currId;

private slots:
	void on_btnNew_clicked();
	void on_btnUpdate_clicked();
	void on_btnShow_clicked();
	void on_cbEN_stateChanged(int);
};

#endif // QLOCALIZATIONDIALOG_H
