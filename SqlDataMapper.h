#ifndef SQLDATAMAPPER_H
#define SQLDATAMAPPER_H

#include <QObject>
#include <QtSql/QSqlQuery>
#include <QDialog>
#include <QList>
#include <QStringList>
#include <QFrame>
#include "SqlDataMapperField.h"

class SqlDataMapper : public QObject
{
	Q_OBJECT

public:
	SqlDataMapper(QObject *parent);
	~SqlDataMapper();

	void setDialog(QDialog *aDialog);
	bool initDialog();

public slots:
	void next();
	void previous();
	void first();
	void last();

private:
	void fillDialog();
	void setFrame(QString & sqlQuery, const QFrame *f);

	QString masterTable;
	QString masterSql;
	QSqlQuery masterQuery;
	QStringList detailTableList;
	QList<SqlDataMapperField*> fieldList;
	
};

#endif // SQLDATAMAPPER_H
