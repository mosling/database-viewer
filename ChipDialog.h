#pragma once

#include "SqlDataDialog.h"

namespace Ui {
class ChipDialog;
}

class ChipDialog : public SqlDataDialog
{
	Q_OBJECT

public:
	ChipDialog(QWidget *parent = 0);
	~ChipDialog();

private:
	Ui::ChipDialog *ui;

};
