#include "SqlDataMapper.h"
#include <QLineEdit>
#include <QComboBox>

SqlDataMapper::SqlDataMapper(QObject *parent)
	: QObject(parent)
{

}

SqlDataMapper::~SqlDataMapper()
{
	qDeleteAll(fieldList);
	fieldList.clear();
}

//! Mit dieser Methode fügen wir die Verbindungen der Dialogfelder
//! eines Frames zur Datenbank hinzu.
//! 
//! \param sqlQuery Aufbau der SQL-Abfrage für den Frame
//! \param f der aktuelle Frame
void SqlDataMapper::setFrame(QString & sqlQuery, const QFrame *f)
{
	bool first = true;
	unsigned int idx = 0;
	SqlDataMapperField *dmf;

	// 1. einfaches Mapping hinzufügen
	QList<QLineEdit*> fl = f->findChildren<QLineEdit*>();
	foreach (QLineEdit *le, fl)
	{
		QString tmpStr = le->whatsThis(); // Feldname
		if (!first) sqlQuery += ", "; else first = false;
		sqlQuery += tmpStr;
		dmf = new SqlDataMapperField(le);
		dmf->fieldIndex = idx++;
		dmf->fieldType = SqlDataMapperField::LineEdit;
		dmf->fieldWidget = le;
		fieldList.append(dmf);
	}

	// 2. Lookup Mapping hinzufügen
	QList<QComboBox*> fll = f->findChildren<QComboBox*>();
	foreach (QComboBox *cb, fll)
	{
		QString tmpStr = cb->whatsThis(); // Feldname master
		if (!first) sqlQuery += ", "; else first = false;
		sqlQuery += tmpStr;
		dmf = new SqlDataMapperField(cb);
		dmf->fieldIndex = idx++;
		dmf->fieldType = SqlDataMapperField::ComboBox;
		dmf->fieldWidget = cb;
		fieldList.append(dmf);

		QStringList lf = cb->statusTip().split(";", QString::SkipEmptyParts);
		if (lf.size() == 4 && lf.at(0) == "FK")
		{
			// erzeuge Fremdschlüssel Lookup
			QString fkSql = QString ("select %1, %2 from %3")
				.arg(lf.at(2)).arg(lf.at(3)).arg(lf.at(1));
			SqlDataMapperRelation *rel = new SqlDataMapperRelation(cb);
			rel->setRelation(fkSql);
			dmf->fieldRelation = rel;
		}
	}
}

/*!
Diese Methode interpretiert einen Dialog mit einem
bestimmten Aufbau als Anzeigedialog eines Datensatzes.
In dieser Methode werden die benötigten Verbindungen 
zur Datenbank erzeugt.
*/
void SqlDataMapper::setDialog(QDialog *aDialog)
{
	// Suche nach der Mastertabelle für den Dialog
	QList<QFrame*> frameList = aDialog->findChildren<QFrame*>();
	foreach (QFrame* f, frameList)
	{
		if (f->statusTip().toUpper() == "MASTER")
		{
			masterTable = f->whatsThis();
			masterSql = "select ";
			setFrame(masterSql, f);
			masterSql += " from " + masterTable;
		}
	}
}

void SqlDataMapper::fillDialog()
{
	if (masterQuery.isActive())
	{
		foreach (SqlDataMapperField *dmf, fieldList)
		{
			bool isNull = masterQuery.value(dmf->fieldIndex).isNull();
			if (dmf->fieldType == SqlDataMapperField::LineEdit)
			{
				QString tmpStr = masterQuery.value(dmf->fieldIndex).toString();
				((QLineEdit*)dmf->fieldWidget)->setText(tmpStr);
			}
			else if (dmf->fieldType == SqlDataMapperField::ComboBox)
			{
				if (isNull)
				{
					((QComboBox*)dmf->fieldWidget)->setCurrentIndex(0);
				}
				else
				{
					dmf->fieldRelation->showIndex(masterQuery.value(dmf->fieldIndex));
				}
			}
		}

	}
}

bool SqlDataMapper::initDialog()
{
	bool bSuccess = true;

	if (masterTable.size() > 0 && masterSql.size() > 0)
	{
		if (masterQuery.exec(masterSql))
		{
			if (masterQuery.isActive()) masterQuery.next();
			fillDialog();
		}
		else
		{
			bSuccess = false;
		}
	}

	return bSuccess;
}

void SqlDataMapper::next()
{
	if ( masterQuery.isActive() && masterQuery.isSelect() )
	{
		masterQuery.next();
		fillDialog();
	}
}

void SqlDataMapper::previous()
{
	if ( masterQuery.isActive() && masterQuery.isSelect() )
	{
		masterQuery.previous();
		fillDialog();
	}
}

void SqlDataMapper::first()
{
	if ( masterQuery.isActive() && masterQuery.isSelect() )
	{
		masterQuery.first();
		fillDialog();
	}
}

void SqlDataMapper::last()
{
	if ( masterQuery.isActive() && masterQuery.isSelect() )
	{
		masterQuery.last();
		fillDialog();
	}
}

